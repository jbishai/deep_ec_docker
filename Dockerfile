FROM continuumio/miniconda3

COPY environment.yml .
RUN conda env create -f environment.yml

RUN echo "conda activate deepec" >> ~/.bashrc
SHELL ["/bin/bash", "--login", "-c"]


ADD deepec.py .
ADD deepec deepec
ADD example example
ADD output output

RUN python ./deepec.py -o output -i ./example/test.fa
